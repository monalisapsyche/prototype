@extends("layout")
@section("content")

    <div class="w3-container w3-dark-grey w3-center">
        <h3>Summary Location</h3>
    </div>
    <br>
    <!--
    <div class="w3-center">
        Month &nbsp;
        <select class="text3" name="se">
            <option value disabled>SELECT</option>
            <option value='1'>January</option>
            <option value='2'>February</option>
            <option value='3'>March</option>
            <option value='4'>April</option>
            <option value='5'>May</option>
            <option value='6'>June</option>
            <option value='7'>July</option>
            <option value='8'>August</option>
            <option value='9'>September</option>
            <option value='10'>October</option>
            <option value='11' selected>November</option>
            <option value='12'>December</option>
        </select>

    </div>

    <br>
    -->
    <table class="w3-table-all table-responsive" border="black">
        <tr>
            <td colspan="4" class="w3-center">Month : {{date('F')}}</td>
        </tr>
        <tr>
            <td class="w3-center">Date</td>
            <td class="w3-center">Point</td>
            <td class="w3-center">Onboard Passenger</td>
            <td class="w3-center">Arrive Passenger</td>
        </tr>


        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'1'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date1ch}}</td>
            <td class="w3-center">{{$date1Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date1toa}}</td>
            <td class="w3-center">{{$date1Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date1ph}}</td>
            <td class="w3-center">{{$date1Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date1samui}}</td>
            <td class="w3-center">{{$date1Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date1surat}}</td>
            <td class="w3-center">{{$date1Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date1Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'2'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date2ch}}</td>
            <td class="w3-center">{{$date2Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date2toa}}</td>
            <td class="w3-center">{{$date2Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date2ph}}</td>
            <td class="w3-center">{{$date2Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date2samui}}</td>
            <td class="w3-center">{{$date2Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date2surat}}</td>
            <td class="w3-center">{{$date2Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date2Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'3'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date3ch}}</td>
            <td class="w3-center">{{$date3Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date3toa}}</td>
            <td class="w3-center">{{$date3Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date3ph}}</td>
            <td class="w3-center">{{$date3Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date3samui}}</td>
            <td class="w3-center">{{$date3Downsamui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date3surat}}</td>
            <td class="w3-center">{{$date3Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date3Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'4'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date4ch}}</td>
            <td class="w3-center">{{$date4Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date4toa}}</td>
            <td class="w3-center">{{$date4Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date4ph}}</td>
            <td class="w3-center">{{$date4Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date4samui}}</td>
            <td class="w3-center">{{$date4Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date4surat}}</td>
            <td class="w3-center">{{$date4Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date4Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'5'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date5ch}}</td>
            <td class="w3-center">{{$date5Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date5toa}}</td>
            <td class="w3-center">{{$date5Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date5ph}}</td>
            <td class="w3-center">{{$date5Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date5samui}}</td>
            <td class="w3-center">{{$date5Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date5surat}}</td>
            <td class="w3-center">{{$date5Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date5Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'6'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date6ch}}</td>
            <td class="w3-center">{{$date6Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date6toa}}</td>
            <td class="w3-center">{{$date6Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date6ph}}</td>
            <td class="w3-center">{{$date6Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date6samui}}</td>
            <td class="w3-center">{{$date6Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date6surat}}</td>
            <td class="w3-center">{{$date6Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date6Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'7'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date7ch}}</td>
            <td class="w3-center">{{$date7Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date7toa}}</td>
            <td class="w3-center">{{$date7Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date7ph}}</td>
            <td class="w3-center">{{$date7Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date7samui}}</td>
            <td class="w3-center">{{$date7Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date7surat}}</td>
            <td class="w3-center">{{$date7Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date7Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'8'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date8ch}}</td>
            <td class="w3-center">{{$date8Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date8toa}}</td>
            <td class="w3-center">{{$date8Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date8ph}}</td>
            <td class="w3-center">{{$date8Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date8samui}}</td>
            <td class="w3-center">{{$date8Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date8surat}}</td>
            <td class="w3-center">{{$date8Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date8Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'9'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date9ch}}</td>
            <td class="w3-center">{{$date9Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date9toa}}</td>
            <td class="w3-center">{{$date9Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date9ph}}</td>
            <td class="w3-center">{{$date9Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date9samui}}</td>
            <td class="w3-center">{{$date9Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date9surat}}</td>
            <td class="w3-center">{{$date9Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date9Total}}</b></td>
        </tr>


        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'10'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date10ch}}</td>
            <td class="w3-center">{{$date10Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date10toa}}</td>
            <td class="w3-center">{{$date10Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date10ph}}</td>
            <td class="w3-center">{{$date10Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date10samui}}</td>
            <td class="w3-center">{{$date10Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date10surat}}</td>
            <td class="w3-center">{{$date10Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date10Total}}</b></td>
        </tr>


        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'11'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date11ch}}</td>
            <td class="w3-center">{{$date11Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date11toa}}</td>
            <td class="w3-center">{{$date11Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date11ph}}</td>
            <td class="w3-center">{{$date11Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date11samui}}</td>
            <td class="w3-center">{{$date11Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date11surat}}</td>
            <td class="w3-center">{{$date11Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date11Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'12'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date12ch}}</td>
            <td class="w3-center">{{$date12Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date12toa}}</td>
            <td class="w3-center">{{$date12Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date12ph}}</td>
            <td class="w3-center">{{$date12Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date12samui}}</td>
            <td class="w3-center">{{$date12Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date12surat}}</td>
            <td class="w3-center">{{$date12Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date12Total}}</b></td>
        </tr>


        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'13'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date13ch}}</td>
            <td class="w3-center">{{$date13Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date13toa}}</td>
            <td class="w3-center">{{$date13Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date13ph}}</td>
            <td class="w3-center">{{$date13Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date13samui}}</td>
            <td class="w3-center">{{$date13Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date13surat}}</td>
            <td class="w3-center">{{$date13Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date13Total}}</b></td>
        </tr>


        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'14'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date14ch}}</td>
            <td class="w3-center">{{$date14Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date14toa}}</td>
            <td class="w3-center">{{$date14Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date14ph}}</td>
            <td class="w3-center">{{$date14Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date14samui}}</td>
            <td class="w3-center">{{$date14Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date14surat}}</td>
            <td class="w3-center">{{$date14Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date14Total}}</b></td>
        </tr>


        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'15'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date15ch}}</td>
            <td class="w3-center">{{$date15Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date15toa}}</td>
            <td class="w3-center">{{$date15Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date15ph}}</td>
            <td class="w3-center">{{$date15Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date15samui}}</td>
            <td class="w3-center">{{$date15Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date15surat}}</td>
            <td class="w3-center">{{$date15Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date15Total}}</b></td>
        </tr>


        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'16'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date16ch}}</td>
            <td class="w3-center">{{$date16Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date16toa}}</td>
            <td class="w3-center">{{$date16Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date16ph}}</td>
            <td class="w3-center">{{$date16Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date16samui}}</td>
            <td class="w3-center">{{$date16Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date16surat}}</td>
            <td class="w3-center">{{$date16Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date16Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'17'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date17ch}}</td>
            <td class="w3-center">{{$date17Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date17toa}}</td>
            <td class="w3-center">{{$date17Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date17ph}}</td>
            <td class="w3-center">{{$date17Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date17samui}}</td>
            <td class="w3-center">{{$date17Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date17surat}}</td>
            <td class="w3-center">{{$date17Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date17Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'18'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date18ch}}</td>
            <td class="w3-center">{{$date18Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date18toa}}</td>
            <td class="w3-center">{{$date18Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date18ph}}</td>
            <td class="w3-center">{{$date18Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date18samui}}</td>
            <td class="w3-center">{{$date18Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date18surat}}</td>
            <td class="w3-center">{{$date18Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date18Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'19'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date19ch}}</td>
            <td class="w3-center">{{$date19Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date19toa}}</td>
            <td class="w3-center">{{$date19Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date19ph}}</td>
            <td class="w3-center">{{$date19Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date19samui}}</td>
            <td class="w3-center">{{$date19Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date19surat}}</td>
            <td class="w3-center">{{$date19Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date19Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'20'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date20ch}}</td>
            <td class="w3-center">{{$date20Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date20toa}}</td>
            <td class="w3-center">{{$date20Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date20ph}}</td>
            <td class="w3-center">{{$date20Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date20samui}}</td>
            <td class="w3-center">{{$date20Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date20surat}}</td>
            <td class="w3-center">{{$date20Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date20Total}}</b></td>
        </tr>


        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'21'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date21ch}}</td>
            <td class="w3-center">{{$date21Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date21toa}}</td>
            <td class="w3-center">{{$date21Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date21ph}}</td>
            <td class="w3-center">{{$date21Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date21samui}}</td>
            <td class="w3-center">{{$date21Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date21surat}}</td>
            <td class="w3-center">{{$date21Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date21Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'22'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date22ch}}</td>
            <td class="w3-center">{{$date22Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date22toa}}</td>
            <td class="w3-center">{{$date22Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date22ph}}</td>
            <td class="w3-center">{{$date22Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date22samui}}</td>
            <td class="w3-center">{{$date22Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date22surat}}</td>
            <td class="w3-center">{{$date22Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date22Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'23'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date23ch}}</td>
            <td class="w3-center">{{$date23Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date23toa}}</td>
            <td class="w3-center">{{$date23Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date23ph}}</td>
            <td class="w3-center">{{$date23Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date23samui}}</td>
            <td class="w3-center">{{$date23Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date23surat}}</td>
            <td class="w3-center">{{$date23Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date23Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'24'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date24ch}}</td>
            <td class="w3-center">{{$date24Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date24toa}}</td>
            <td class="w3-center">{{$date24Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date24ph}}</td>
            <td class="w3-center">{{$date24Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date24samui}}</td>
            <td class="w3-center">{{$date24Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date24surat}}</td>
            <td class="w3-center">{{$date24Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date24Total}}</b></td>
        </tr>


        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'25'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date25ch}}</td>
            <td class="w3-center">{{$date25Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date25toa}}</td>
            <td class="w3-center">{{$date25Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date25ph}}</td>
            <td class="w3-center">{{$date25Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date25samui}}</td>
            <td class="w3-center">{{$date25Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date25surat}}</td>
            <td class="w3-center">{{$date25Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date25Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'26'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date26ch}}</td>
            <td class="w3-center">{{$date26Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date26toa}}</td>
            <td class="w3-center">{{$date26Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date26ph}}</td>
            <td class="w3-center">{{$date26Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date26samui}}</td>
            <td class="w3-center">{{$date26Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date26surat}}</td>
            <td class="w3-center">{{$date26Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date26Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'27'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date27ch}}</td>
            <td class="w3-center">{{$date27Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date27toa}}</td>
            <td class="w3-center">{{$date27Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date27ph}}</td>
            <td class="w3-center">{{$date27Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date27samui}}</td>
            <td class="w3-center">{{$date27Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date27surat}}</td>
            <td class="w3-center">{{$date27Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date27Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'28'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date28ch}}</td>
            <td class="w3-center">{{$date28Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date28toa}}</td>
            <td class="w3-center">{{$date28Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date28ph}}</td>
            <td class="w3-center">{{$date28Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date28samui}}</td>
            <td class="w3-center">{{$date28Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date28surat}}</td>
            <td class="w3-center">{{$date28Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date28Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'29'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date29ch}}</td>
            <td class="w3-center">{{$date29Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date29toa}}</td>
            <td class="w3-center">{{$date29Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date29ph}}</td>
            <td class="w3-center">{{$date29Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date29samui}}</td>
            <td class="w3-center">{{$date29Downsmui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date29surat}}</td>
            <td class="w3-center">{{$date29Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date29Total}}</b></td>
        </tr>

        <tr>
            <td rowspan="6">{{date('Y-m').'-'.'30'}}</td>
        </tr>
        <tr>
            <td>Chumphon</td>
            <td class="w3-center">{{$date30ch}}</td>
            <td class="w3-center">{{$date30Downch}}</td>
        </tr>
        <tr>
            <td>Tao</td>
            <td class="w3-center">{{$date30toa}}</td>
            <td class="w3-center">{{$date30Downtao}}</td>
        </tr>
        <tr>
            <td>Phangan</td>
            <td class="w3-center">{{$date30ph}}</td>
            <td class="w3-center">{{$date30Downph}}</td>
        </tr>
        <tr>
            <td>Samui</td>
            <td class="w3-center">{{$date30samui}}</td>
            <td class="w3-center">{{$date30Downsamui}}</td>
        </tr>
        <tr>
            <td>Surat</td>
            <td class="w3-center">{{$date30surat}}</td>
            <td class="w3-center">{{$date30Downsarat}}</td>
        </tr>
        <tr>
            <td colspan="8">Passenger (Daily day) : <b>{{$date30Total}}</b></td>
        </tr>


        @if(date('Y-m') == '2017-12')
            <tr>
                <td rowspan="6">{{date('Y-m').'-'.'31'}}</td>
            </tr>
            <tr>
                <td>Chumphon</td>
                <td class="w3-center">{{$date31ch}}</td>
                <td class="w3-center">{{$date30Downch}}</td>
            </tr>
            <tr>
                <td>Tao</td>
                <td class="w3-center">{{$date31toa}}</td>
                <td class="w3-center">{{$date31Downtao}}</td>
            </tr>
            <tr>
                <td>Phangan</td>
                <td class="w3-center">{{$date31ph}}</td>
                <td class="w3-center">{{$date31Downph}}</td>
            </tr>
            <tr>
                <td>Samui</td>
                <td class="w3-center">{{$date31samui}}</td>
                <td class="w3-center">{{$date31Downsamui}}</td>
            </tr>
            <tr>
                <td>Surat</td>
                <td class="w3-center">{{$date31surat}}</td>
                <td class="w3-center">{{$date31Downsarat}}</td>
            </tr>
            <tr>
                <td colspan="8">Passenger (Daily day) : <b>{{$date31Total}}</b></td>
            </tr>

        @endif
    </table>

    <br><br>













@endsection