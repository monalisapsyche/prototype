@extends("layout")
@section("content")





    <div class="w3-container w3-dark-grey w3-center">
        <h3>Table Data</h3>
    </div>
    <br>
    <form name="frm" method="post" action="{{ url('/dataTable') }}"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="w3-center">
            Date: <input type="text" name="serDate" id="datepicker" placeholder="{{date('Y-m-d')}}"> &nbsp;
            <button class="w3-button w3-black w3-hover-blue" name="search" type="submit">SEARCH</button>
        </div>
    </form>
    <br>
    <table class="w3-table-all table-responsive" border="black">
        <tr>
            <td rowspan="2" class="w3-center"><br>
                Boat name
            </td>
            <td rowspan="2" class="w3-center"><br>
                Route
            </td>
            <td rowspan="2" class="w3-center"><br>
                Date
            </td>
            <td rowspan="2" class="w3-center"><br>
                Start
            </td>
            <td rowspan="2" class="w3-center"><br>
                Quota
            </td>
            <td colspan="3" class="w3-center">
                Reservation
            </td>
            <td colspan="5" class="w3-center">
                Destination
            </td>
        </tr>
        <tr>
            <td class="w3-center">Booking</td>
            <td class="w3-center">Port</td>
            <td class="w3-center">Total</td>


            <td class="w3-center"><span id="01">Chumphon</span></td>
            <td class="w3-center"><span id="02">Tao</span></td>
            <td class="w3-center"><span id="03">Phangan</span></td>
            <td class="w3-center"><span id="04">Samui</span></td>
            <td class="w3-center"><span id="05">Surat</span></td>
        </tr>
        @foreach($selQuota as $quota)
            <tr>
                <td>{{$quota->boat_name}}</td>
                <td>{{$quota->route_name}}</td>
                <td>{{$quota->quota_date}}</td>
                <td>{{$quota->start_name}}</td>
                <td class="w3-center">{{$quota->quota_number}}</td>
                <td class="w3-center">{{$quota->quota_booking}}</td>
                <td class="w3-center">{{$quota->quota_port}}</td>
                <td class="w3-center"><b>{{$quota->quota_total}}</b></td>


                <td class="w3-center">{{$quota->des_ch}}</td>
                <td class="w3-center">{{$quota->des_tao}}</td>
                <td class="w3-center">{{$quota->des_ph}}</td>
                <td class="w3-center">{{$quota->des_samui}}</td>
                <td class="w3-center">{{$quota->des_surat}}</td>
            </tr>
        @endforeach
    </table>

    <br>
    Passenger (Daily day) : <b>{{$total}}</b>

    <br><br>

    <script>
        $(function () {
            $("#datepicker").datepicker({
                dateFormat: "yy-mm-dd"
                , showButtonPanel: true

            });
        });
    </script>

@endsection