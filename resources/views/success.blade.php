@extends("layout")
@section("content")


    <div class="w3-container w3-white w3-center">
        <div class="w3-center">
            <h2>Successfully saved</h2>
        </div>
        <br>
        <div class="w3-center">
            <a href="{{URL::to('/')}}" class="w3-button w3-black w3-hover-blue" name="00">HOME</a>
        </div>
        <br>
    </div>


@endsection