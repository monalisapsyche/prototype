@extends("layout")
@section("content")

    <div class="w3-container w3-dark-grey w3-center">
        <h3>Quota Management</h3>
    </div>

    <form name="frm" method="post" action="{{ url('/insertQuota') }}"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div style="margin-top: 2%">

            <div class="w3-center">

                <b>Boat Name : &nbsp;</b>

                <select class="text3" id="seat" onchange="selSeat()" name="boat_ID" required>
                    <option value disabled selected>SELECT</option>
                    @foreach($selBoat as $boat)
                        <option value="{{$boat->boat_ID}}">{{$boat->boat_name}}</option>
                    @endforeach
                </select>
                <b>Seat : </b><input class="text2" id="showSeat">
                <br> <br>
                <b>Route :</b>
                <select class="text3" id="route" onchange="routeChange()" name="route_ID" required>
                    <option value disabled selected>SELECT</option>
                    @foreach($selRoute as $route)
                        <option value="{{$route->route_ID}}">{{$route->route_name}}</option>
                    @endforeach
                </select>
                <b>Date :</b>
                <input class="text3" id="dateInput" name="quota_date" value="{{Date("Y-m-d")}}"><br>

            </div>

        </div>


        <br>
        <table class="w3-table-all table-responsive" border="black">
            <tr>
                <td rowspan="2" class="w3-center"><br>
                    Start
                </td>
                <td rowspan="2" class="w3-center"><br>
                    Quota
                </td>
                <td rowspan="2" class="w3-center"><br>
                    Blank
                </td>
                <td colspan="3" class="w3-center">
                    Reservation
                </td>
                <td colspan="5" class="w3-center">
                    Destination
                </td>
            </tr>
            <tr>
                <td class="w3-center">Booking</td>
                <td class="w3-center">Port</td>
                <td class="w3-center">Total</td>


                <td class="w3-center"><span id="01">Chumphon</span></td>
                <td class="w3-center"><span id="02">Tao</span></td>
                <td class="w3-center"><span id="03">Phangan</span></td>
                <td class="w3-center"><span id="04">Samui</span></td>
                <td class="w3-center"><span id="05">Surat</span></td>
            </tr>

            <tr>
                <td>
                    <select name="start_ID" id="ddlStart" onchange="startQuota()" required>
                        <option value disabled selected>SELECT</option>
                        @foreach($selStart as $start)
                            <option value="{{$start->start_ID}}">{{$start->start_name}}</option>
                        @endforeach
                    </select>
                </td>
                <td class="w3-center">
                    <input class="w3-input w3-border" name="quota_number" id="showQuota" readonly>

                    <span class="w3-button w3-black w3-hover-blue w3-tiny" onclick="popup()">Request Quota</span>

                </td>

                <td class="w3-center"><input class="w3-input w3-border" id="showQuota02" name="blank" readonly></td>


                <td class="w3-center"><input class="w3-input w3-border" name="quota_booking" onKeyUp="chk()" required></td>
                <td class="w3-center"><input class="w3-input w3-border" name="quota_port" onKeyUp="chk()" required></td>
                <td class="w3-center">
                    <input class="w3-input w3-border" name="quota_total">

                </td>


                <td class="w3-center"><input class="w3-input w3-border" name="des_ch" required></td>
                <td class="w3-center"><input class="w3-input w3-border" name="des_tao" required></td>
                <td class="w3-center"><input class="w3-input w3-border" name="des_ph" required></td>
                <td class="w3-center"><input class="w3-input w3-border" name="des_samui" required></td>
                <td class="w3-center"><input class="w3-input w3-border" name="des_surat" required></td>
            </tr>

        </table>
        <div class="w3-center">
            <strong style="color: red"> ***กรุณาใส่ข้อมูลให้ครบทุกช่อง ช่องไหนไม่มีข้อมูลให้ใส่ เลข 0 ***</strong>
        </div>
        <br>
        <div class="w3-center">
            <button class="w3-button w3-black w3-hover-blue" type="submit" name="00">SUBMIT</button>
        </div>

        <br><br>

    </form>






    <script type="text/javascript">
        function popup() {
            w2popup.open({
                title: '<b>Request Quota</b>',
                body: '<div class="popup"><br><br>Number : <input type="text" > <br> Remark : <textarea></textarea><br>' +
                ' <button class="w3-button w3-center w3-black w3-hover-blue">OK</button> </div>'
            });
        }


        function chk() {

            var a1 = parseFloat(document.frm.quota_booking.value);
            var a2 = parseFloat(document.frm.quota_port.value);
            //  var a3 = parseFloat(document.frm.blank.value);
            // var a4 = parseFloat(document.frm.quota_total.value);
            document.frm.quota_total.value = a1 + a2; //---- เปลี่ยนเอาจะ + - * /
            //     document.frm.blank.value = a3 - a4; //---- เปลี่ยนเอาจะ + - * /


        }

        function selSeat() {
            var s = document.getElementById("seat").value;
            //    var x = document.getElementById("ddlStart").value;
            //   var r = document.getElementById("route").value;

            if (s == '1' || s == '3') {
                document.getElementById("showSeat").value = "600";
            } else {
                document.getElementById("showSeat").value = "450";
            }


        }


        function startQuota() {
            var x = document.getElementById("ddlStart").value;
            var s = document.getElementById("seat").value;
            if (s == '1' || s == '3') {
                $n1 = document.getElementById("showQuota").value = "600";
                $n2 = document.getElementById("showQuota02").value = "600";
            } else {
                document.getElementById("showQuota").value = "450";
                document.getElementById("showQuota02").value = "450";
            }

            if (x == '1' || x == '5') {
                document.getElementById("showQuota").value = $n1;
                document.getElementById("showQuota02").value = $n2;

            } else {
                document.getElementById("showQuota").value = "200";
                document.getElementById("showQuota02").value = "200";
            }
        }

        function routeChange() {
            var r = document.getElementById("route").value;
            // var x = document.getElementById("ddlStart").value;
            if (r == '1') {

            }
        }

        ////วันที่ ห้ามเลือกย้อนหลังากปัจจุบัน /////

        $(function () {

            $("#dateInput").datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: new Date(),
                maxDate: new Date('Y-m-d'),
                numberOfMonths: 2,
            });
        });


    </script>

@endsection