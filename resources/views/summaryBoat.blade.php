@extends("layout")
@section("content")

    <div class="w3-container w3-dark-grey w3-center">
        <h3>Summary Boat</h3>
    </div>
    <br>

    <form method="post" action="{{ url('/summaryBoat') }}"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="w3-center">
            Month &nbsp;
            <select class="text4" name="se">
                <option value disabled selected>SELECT</option>
                <option value='1'>January</option>
                <option value='2'>February</option>
                <option value='3'>March</option>
                <option value='4'>April</option>
                <option value='5'>May</option>
                <option value='6'>June</option>
                <option value='7'>July</option>
                <option value='8'>August</option>
                <option value='9'>September</option>
                <option value='10'>October</option>
                <option value='11'>November</option>
                <option value='12'>December</option>
            </select>


            <button class="w3-button w3-black w3-hover-blue" name="search" type="submit">SEARCH</button>


        </div>
    </form>
    <br>
    <table class="w3-table-all table-responsive" border="black">
        <tr>
            <td rowspan="2" class="w3-center"><br>Boat Name</td>
            <td rowspan="2" class="w3-center"><br>Route</td>
            <td colspan="15" class="w3-center">Month : {{date('F')}}</td>
            <td rowspan="2" class="w3-center"><br>Round Number</td>
        </tr>

        <tr>
            @for ($i = 1; $i <= 15; $i++)
                <td class="w3-center">{{$i}}</td>
            @endfor
        </tr>

        <tr>
            <td>Royal Jet 3</td>
            <td><span class="w3-text-green"> Chumphon - Surat</span></td>

            @for($i=0;$i<=14;$i++)
                <td class="w3-center">{{$selBoat[$i]}}</td>
        @endfor
        <!--  <td class="w3-center">{{$countBoat1}}</td> -->
            <td class="w3-center">{{$countBoat1}}</td>
        </tr>

        <tr>
            <td></td>
            <td><span class="w3-text-red"> Surat - Chumphon</span></td>
            @for($i=0;$i<=14;$i++)
                <td class="w3-center">{{$selBoatSu01[$i]}}</td>
            @endfor
            <td class="w3-center">{{$countBoat2}}</td>
        </tr>
        <tr>
            <td>Royal Jet 5</td>
            <td><span class="w3-text-green"> Chumphon - Surat</span></td>
            @for($i=0;$i<=14;$i++)
                <td class="w3-center">{{$selR501[$i]}}</td>
            @endfor
            <td class="w3-center">{{$countBoat3}}</td>
        </tr>
        <tr>
            <td></td>
            <td><span class="w3-text-red"> Surat - Chumphon</span></td>
            @for($i=0;$i<=14;$i++)
                <td class="w3-center">{{$selR5Su01[$i]}}</td>
            @endfor
            <td class="w3-center">{{$countBoat4}}</td>
        </tr>
        <tr>
            <td>Royal Jet 9</td>
            <td><span class="w3-text-green">Chumphon - Surat</span></td>
            @for($i=0;$i<=14;$i++)
                <td class="w3-center">{{$selR901[$i]}}</td>
            @endfor
            <td class="w3-center">{{$countBoat5}}</td>
        </tr>
        <tr>
            <td></td>
            <td><span class="w3-text-red">Surat - Chumphon</span></td>
            @for($i=0;$i<=14;$i++)
                <td class="w3-center">{{$selR9Su01[$i]}}</td>
            @endfor
            <td class="w3-center">{{$countBoat6}}</td>
    </table>
    <br>



    <table class="w3-table-all table-responsive" border="black">
        @if($sum>30)
            <tr>
                <td rowspan="2" class="w3-center"><br>Name Boat</td>
                <td rowspan="2" class="w3-center"><br>Route</td>
                <td colspan="16" class="w3-center">Month : {{date('F')}}</td>
                <td rowspan="2" class="w3-center"><br>Round Number</td>
            </tr>
        @elseif($sum<30)
            <tr>
                <td rowspan="2" class="w3-center"><br>Name Boat</td>
                <td rowspan="2" class="w3-center"><br>Route</td>
                <td colspan="14" class="w3-center">Month : November</td>
                <td rowspan="2" class="w3-center"><br>Round Number</td>
            </tr>
        @else
            <tr>
                <td rowspan="2" class="w3-center"><br>Name Boat</td>
                <td rowspan="2" class="w3-center"><br>Route</td>
                <td colspan="15" class="w3-center">Month : November</td>
                <td rowspan="2" class="w3-center"><br>Round Number</td>
            </tr>
        @endif


        <tr>
            @if($sum>30)
                @for ($i = 16; $i <= 31; $i++)
                    <td class="w3-center">{{$i}}</td>
                @endfor
            @else
                @for($i=16;$i<=30;$i++)
                    <td class="w3-center">{{$i}}</td>
                @endfor
            @endif
        </tr>


        <tr>
            <td>Royal Jet 3</td>
            <td><span class="w3-text-green"> Chumphon - Surat</span></td>

            @if($sum>30)
                @for($i=15;$i<=30;$i++)
                    <td class="w3-center">{{$selBoat[$i]}}</td>
                @endfor
            @else
                @for($i=15;$i<=29;$i++)
                    <td class="w3-center">{{$selBoat[$i]}}</td>
                @endfor
            @endif
            <td class="w3-center">{{$countBoat7}}</td>
        </tr>
        <tr>
            <td></td>
            <td><span class="w3-text-red"> Surat - Chumphon</span></td>
            @if($sum>30)
                @for($i=15;$i<=30;$i++)
                    <td class="w3-center">{{$selBoatSu01[$i]}}</td>
                @endfor
            @else
                @for($i=15;$i<=29;$i++)
                    <td class="w3-center">{{$selBoatSu01[$i]}}</td>
                @endfor
            @endif
            <td class="w3-center">{{$countBoat8}}</td>
        </tr>
        <tr>
            <td>Royal Jet 5</td>
            <td><span class="w3-text-green"> Chumphon - Surat</span></td>
            @if($sum>30)
                @for($i=15;$i<=30;$i++)
                    <td class="w3-center">{{$selR501[$i]}}</td>
                @endfor
            @else
                @for($i=15;$i<=29;$i++)
                    <td class="w3-center">{{$selR501[$i]}}</td>
                @endfor
            @endif
            <td class="w3-center">{{$countBoat9}}</td>
        </tr>
        <tr>
            <td></td>
            <td><span class="w3-text-red"> Surat - Chumphon</span></td>
            @if($sum>30)
                @for($i=15;$i<=30;$i++)
                    <td class="w3-center">{{$selR5Su01[$i]}}</td>
                @endfor
            @else
                @for($i=15;$i<=29;$i++)
                    <td class="w3-center">{{$selR5Su01[$i]}}</td>
                @endfor
            @endif
            <td class="w3-center">{{$countBoat10}}</td>
        </tr>
        <tr>
            <td>Royal Jet 9</td>
            <td><span class="w3-text-green">Chumphon - Surat</span></td>
            @if($sum>30)
                @for($i=15;$i<=30;$i++)
                    <td class="w3-center">{{$selR901[$i]}}</td>
                @endfor
            @else
                @for($i=15;$i<=29;$i++)
                    <td class="w3-center">{{$selR901[$i]}}</td>
                @endfor
            @endif
            <td class="w3-center">{{$countBoat11}}</td>
        </tr>
        <tr>
            <td></td>
            <td><span class="w3-text-red">Surat - Chumphon</span></td>
            @if($sum>30)
                @for($i=15;$i<=30;$i++)
                    <td class="w3-center">{{$selR9Su01[$i]}}</td>
                @endfor
            @else
                @for($i=15;$i<=29;$i++)
                    <td class="w3-center">{{$selR9Su01[$i]}}</td>
                @endfor
            @endif
            <td class="w3-center">{{$countBoat12}}</td>
        </tr>
    </table>
    <br>
    <b>Summary round number (Monthly) </b>
    <table>
        <tr>
            <td class="w3-right">Boat Name :</td>
            <td>Royal Jet 3</td>
        </tr>
        <tr>
            <td class="w3-right">Route :</td>
            <td>Chumphon - Surat = <b>{{$R301}}</b></td>
        </tr>
        <tr>
            <td></td>
            <td>Surat - Chumphon = <b>{{$R302}}</b></td>
        </tr>
        <tr>
            <td class="w3-right">Total : <b>{{$TotalR3}}</b></td>
            <td></td>
        </tr>
    </table>
    <br>
    <table>
        <tr>
            <td class="w3-right">Boat Name :</td>
            <td>Royal Jet 5</td>
        </tr>
        <tr>
            <td class="w3-right">Route :</td>
            <td>Chumphon - Surat = <b>{{$R501}}</b></td>
        </tr>
        <tr>
            <td></td>
            <td>Surat - Chumphon = <b>{{$R502}}</b></td>
        </tr>
        <tr>
            <td class="w3-right">Total : <b>{{$TotalR5}}</b></td>
            <td></td>
        </tr>
    </table>
    <br>
    <table>
        <tr>
            <td class="w3-right">Boat Name :</td>
            <td>Royal Jet 9</td>
        </tr>
        <tr>
            <td class="w3-right">Route :</td>
            <td>Chumphon - Surat = <b>{{$R901}}</b></td>
        </tr>
        <tr>
            <td></td>
            <td>Surat - Chumphon = <b>{{$R902}}</b></td>
        </tr>
        <tr>
            <td class="w3-right">Total : <b>{{$TotalR9}}</b></td>
            <td></td>
        </tr>
    </table>
    <br>


    Summary of total passengers : <b>{{$total}}</b>
    <br><br>
    <br><br>
@endsection