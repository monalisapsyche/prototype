<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('quota');
});
Route::get('test02', function () {
    return view('test02');
});



Route::get('/', 'Controller@SelectData');
Route::post('/insertQuota', 'insertQuota@insertQuota');
Route::get('/dataTable', 'SelectDataTable@SelectDataTable');
Route::post('/dataTable', 'SelectDataTable@SelectDataTable');
Route::get('/summaryBoat', 'selectSummaryBoat@summaryBoat');
Route::get('/summaryLocation', 'selectSummaryLocation@summaryLocation');


Route::post('/summaryBoat', 'selectSummaryBoat@summaryBoat');
