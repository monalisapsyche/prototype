<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SelectDataTable extends Controller
{
    function SelectDataTable(Request $request)
    {


        if (isset($request['search'])) {

            $serstrat = $request['serDate'];

            $selQuota = DB::table('quota')
                ->leftJoin('boat', 'boat.boat_ID', '=', 'quota.boat_ID')
                ->leftJoin('route', 'route.route_ID', '=', 'quota.route_ID')
                ->leftJoin('start', 'start.start_ID', '=', 'quota.start_ID')
                ->where('quota_date', '=', $serstrat)
                ->get();

            $total = DB::table('quota')->where('quota_date', '=', $serstrat)->sum('quota_total');
            $summaryTotal = DB::table('quota')
                ->sum('quota_total');


        } else {

            $selQuota = DB::table('quota')
                ->leftJoin('boat', 'boat.boat_ID', '=', 'quota.boat_ID')
                ->leftJoin('route', 'route.route_ID', '=', 'quota.route_ID')
                ->leftJoin('start', 'start.start_ID', '=', 'quota.start_ID')
                ->where('quota_date', date("Y-m-d"))->get();
            $total = DB::table('quota')->where('quota_date', '=', date("Y-m-d"))->sum('quota_total');

            $summaryTotal = DB::table('quota')
                ->sum('quota_total');

        }
        return view('dataTable',
            [
                'selQuota' => $selQuota,
                'total' => $total,
                'summaryTotal' => $summaryTotal,
            ]);

    }

}
