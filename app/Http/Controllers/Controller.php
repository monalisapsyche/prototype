<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function SelectData()
    {
        $selBoat = DB::table('boat')->get();
        $selRoute = DB::table('route')->get();
        $selStart = DB::table('start')->get();
        $summaryTotal = DB::table('quota')
            ->sum('quota_total');

        return view('quota',
            [
                'selBoat' => $selBoat,
                'selRoute' => $selRoute,
                'selStart' => $selStart,
                'summaryTotal' => $summaryTotal,
            ]);
    }

}
