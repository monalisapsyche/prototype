<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class insertQuota extends Controller
{

    function insertQuota(Request $request)
    {

        $boat_ID = $request['boat_ID'];
        $route_ID = $request['route_ID'];
        $start_ID = $request['start_ID'];
        $quota_date = $request['quota_date'];
        $quota_number = $request['quota_number'];

        $quota_booking = $request['quota_booking'];
        $quota_port = $request['quota_port'];
        $quota_total = $request['quota_total'];
        $des_ch = $request['des_ch'];
        $des_tao = $request['des_tao'];

        $des_ph = $request['des_ph'];
        $des_samui = $request['des_samui'];
        $des_surat = $request['des_surat'];

        $data1 = array('boat_ID' => $boat_ID,
            'route_ID' => $route_ID,
            'start_ID' => $start_ID,
            'quota_date' => $quota_date,
            'quota_number' => $quota_number,
            'quota_booking' => $quota_booking,
            'quota_port' => $quota_port,

            'quota_total' => $quota_total,
            'des_ch' => $des_ch,
            'des_tao' => $des_tao,
            'des_ph' => $des_ph,
            'des_samui' => $des_samui,
            'des_surat' => $des_surat,
        );

        DB::table('quota')->insert($data1);


        $summaryTotal = DB::table('quota')
            ->sum('quota_total');

        return view('success',
            ['summaryTotal' => $summaryTotal,]);




    }

}
