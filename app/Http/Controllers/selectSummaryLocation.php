<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class selectSummaryLocation extends Controller
{
    function summaryLocation()
    {
        $summaryTotal = DB::table('quota')
            ->sum('quota_total');

        $date1ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-1'));
            })->sum('quota_total');

        $date1toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-1'));
            })->sum('quota_total');

        $date1ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-1'));
            })->sum('quota_total');

        $date1samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-1'));
            })->sum('quota_total');

        $date1surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-1'));
            })->sum('quota_total');


        $date1Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-1'));
            })->sum('des_ch');

        $date1Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-1'));
            })->sum('des_tao');

        $date1Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-1'));
            })->sum('des_ph');

        $date1Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-1'));
            })->sum('des_samui');

        $date1Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-1'));
            })->sum('des_surat');

        $date1Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-1'))
            ->sum('quota_total');


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $date2ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-2'));
            })->sum('quota_total');

        $date2toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-2'));
            })->sum('quota_total');

        $date2ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-2'));
            })->sum('quota_total');

        $date2samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-2'));
            })->sum('quota_total');

        $date2surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-2'));
            })->sum('quota_total');


        $date2Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-2'));
            })->sum('des_ch');

        $date2Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-2'));
            })->sum('des_tao');

        $date2Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-2'));
            })->sum('des_ph');

        $date2Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-2'));
            })->sum('des_samui');

        $date2Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-2'));
            })->sum('des_surat');

        $date2Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-2'))
            ->sum('quota_total');


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $date3ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-3'));
            })->sum('quota_total');

        $date3toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-3'));
            })->sum('quota_total');

        $date3ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-3'));
            })->sum('quota_total');

        $date3samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-3'));
            })->sum('quota_total');

        $date3surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-3'));
            })->sum('quota_total');


        $date3Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-3'));
            })->sum('des_ch');

        $date3Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-3'));
            })->sum('des_tao');

        $date3Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-3'));
            })->sum('des_ph');

        $date3Downsamui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-3'));
            })->sum('des_samui');

        $date3Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-3'));
            })->sum('des_surat');

        $date3Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-3'))
            ->sum('quota_total');


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $date4ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-4'));
            })->sum('quota_total');

        $date4toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-4'));
            })->sum('quota_total');

        $date4ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-4'));
            })->sum('quota_total');

        $date4samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-4'));
            })->sum('quota_total');

        $date4surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-4'));
            })->sum('quota_total');


        $date4Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-4'));
            })->sum('des_ch');

        $date4Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-4'));
            })->sum('des_tao');

        $date4Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-4'));
            })->sum('des_ph');

        $date4Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-4'));
            })->sum('des_samui');

        $date4Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-4'));
            })->sum('des_surat');

        $date4Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-4'))
            ->sum('quota_total');


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $date5ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-5'));
            })->sum('quota_total');

        $date5toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-5'));
            })->sum('quota_total');

        $date5ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-5'));
            })->sum('quota_total');

        $date5samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-5'));
            })->sum('quota_total');

        $date5surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-5'));
            })->sum('quota_total');


        $date5Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-6'));
            })->sum('des_ch');

        $date5Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-5'));
            })->sum('des_tao');

        $date5Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-5'));
            })->sum('des_ph');

        $date5Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-5'));
            })->sum('des_samui');

        $date5Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-5'));
            })->sum('des_surat');

        $date5Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-5'))
            ->sum('quota_total');


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date6ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-6'));
            })->sum('quota_total');

        $date6toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-6'));
            })->sum('quota_total');

        $date6ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-6'));
            })->sum('quota_total');

        $date6samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-6'));
            })->sum('quota_total');

        $date6surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-6'));
            })->sum('quota_total');


        $date6Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-6'));
            })->sum('des_ch');

        $date6Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-6'));
            })->sum('des_tao');

        $date6Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-6'));
            })->sum('des_ph');

        $date6Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-6'));
            })->sum('des_samui');

        $date6Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-6'));
            })->sum('des_surat');

        $date6Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-6'))
            ->sum('quota_total');


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date7ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-7'));
            })->sum('quota_total');

        $date7toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-7'));
            })->sum('quota_total');

        $date7ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-7'));
            })->sum('quota_total');

        $date7samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-7'));
            })->sum('quota_total');

        $date7surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-7'));
            })->sum('quota_total');


        $date7Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-7'));
            })->sum('des_ch');

        $date7Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-7'));
            })->sum('des_tao');

        $date7Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-7'));
            })->sum('des_ph');

        $date7Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-7'));
            })->sum('des_samui');

        $date7Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-7'));
            })->sum('des_surat');

        $date7Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-7'))
            ->sum('quota_total');

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        $date8ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-8'));
            })->sum('quota_total');

        $date8toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-8'));
            })->sum('quota_total');

        $date8ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-8'));
            })->sum('quota_total');

        $date8samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-8'));
            })->sum('quota_total');

        $date8surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-8'));
            })->sum('quota_total');


        $date8Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-8'));
            })->sum('des_ch');

        $date8Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-8'));
            })->sum('des_tao');

        $date8Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-8'));
            })->sum('des_ph');

        $date8Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-8'));
            })->sum('des_samui');

        $date8Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-8'));
            })->sum('des_surat');

        $date8Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-8'))
            ->sum('quota_total');

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        $date9ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-9'));
            })->sum('quota_total');

        $date9toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-9'));
            })->sum('quota_total');

        $date9ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-9'));
            })->sum('quota_total');

        $date9samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-9'));
            })->sum('quota_total');

        $date9surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-9'));
            })->sum('quota_total');


        $date9Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-9'));
            })->sum('des_ch');

        $date9Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-9'));
            })->sum('des_tao');

        $date9Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-9'));
            })->sum('des_ph');

        $date9Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-9'));
            })->sum('des_samui');

        $date9Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-9'));
            })->sum('des_surat');

        $date9Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-9'))
            ->sum('quota_total');

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date10ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-10'));
            })->sum('quota_total');

        $date10toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-10'));
            })->sum('quota_total');

        $date10ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-10'));
            })->sum('quota_total');

        $date10samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-10'));
            })->sum('quota_total');

        $date10surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-10'));
            })->sum('quota_total');


        $date10Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-10'));
            })->sum('des_ch');

        $date10Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-10'));
            })->sum('des_tao');

        $date10Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-10'));
            })->sum('des_ph');

        $date10Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-10'));
            })->sum('des_samui');

        $date10Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-10'));
            })->sum('des_surat');

        $date10Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-10'))
            ->sum('quota_total');

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date11ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-11'));
            })->sum('quota_total');

        $date11toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-11'));
            })->sum('quota_total');

        $date11ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-11'));
            })->sum('quota_total');

        $date11samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-11'));
            })->sum('quota_total');

        $date11surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-11'));
            })->sum('quota_total');


        $date11Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-11'));
            })->sum('des_ch');

        $date11Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-11'));
            })->sum('des_tao');

        $date11Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-11'));
            })->sum('des_ph');

        $date11Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-11'));
            })->sum('des_samui');

        $date11Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-11'));
            })->sum('des_surat');

        $date11Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-11'))
            ->sum('quota_total');

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date12ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-12'));
            })->sum('quota_total');

        $date12toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-12'));
            })->sum('quota_total');

        $date12ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-12'));
            })->sum('quota_total');

        $date12samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-12'));
            })->sum('quota_total');

        $date12surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-12'));
            })->sum('quota_total');


        $date12Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-12'));
            })->sum('des_ch');

        $date12Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-12'));
            })->sum('des_tao');

        $date12Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-12'));
            })->sum('des_ph');

        $date12Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-12'));
            })->sum('des_samui');

        $date12Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-12'));
            })->sum('des_surat');

        $date12Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-12'))
            ->sum('quota_total');

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date13ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-13'));
            })->sum('quota_total');

        $date13toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-13'));
            })->sum('quota_total');

        $date13ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-13'));
            })->sum('quota_total');

        $date13samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-13'));
            })->sum('quota_total');

        $date13surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-13'));
            })->sum('quota_total');


        $date13Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-13'));
            })->sum('des_ch');

        $date13Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-13'));
            })->sum('des_tao');

        $date13Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-13'));
            })->sum('des_ph');

        $date13Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-13'));
            })->sum('des_samui');

        $date13Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-13'));
            })->sum('des_surat');

        $date13Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-13'))
            ->sum('quota_total');

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date14ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-14'));
            })->sum('quota_total');

        $date14toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-14'));
            })->sum('quota_total');

        $date14ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-14'));
            })->sum('quota_total');

        $date14samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-14'));
            })->sum('quota_total');

        $date14surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-14'));
            })->sum('quota_total');


        $date14Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-14'));
            })->sum('des_ch');

        $date14Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-14'));
            })->sum('des_tao');

        $date14Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-14'));
            })->sum('des_ph');

        $date14Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-14'));
            })->sum('des_samui');

        $date14Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-14'));
            })->sum('des_surat');

        $date14Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-14'))
            ->sum('quota_total');

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date15ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-15'));
            })->sum('quota_total');

        $date15toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-15'));
            })->sum('quota_total');

        $date15ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-15'));
            })->sum('quota_total');

        $date15samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-15'));
            })->sum('quota_total');

        $date15surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-15'));
            })->sum('quota_total');


        $date15Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-15'));
            })->sum('des_ch');

        $date15Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-15'));
            })->sum('des_tao');

        $date15Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-15'));
            })->sum('des_ph');

        $date15Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-15'));
            })->sum('des_samui');

        $date15Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-15'));
            })->sum('des_surat');

        $date15Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-15'))
            ->sum('quota_total');

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date16ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-16'));
            })->sum('quota_total');

        $date16toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-16'));
            })->sum('quota_total');

        $date16ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-16'));
            })->sum('quota_total');

        $date16samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-16'));
            })->sum('quota_total');

        $date16surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-16'));
            })->sum('quota_total');


        $date16Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-16'));
            })->sum('des_ch');

        $date16Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-16'));
            })->sum('des_tao');

        $date16Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-16'));
            })->sum('des_ph');

        $date16Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-16'));
            })->sum('des_samui');

        $date16Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-16'));
            })->sum('des_surat');

        $date16Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-16'))
            ->sum('quota_total');

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date17ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-17'));
            })->sum('quota_total');

        $date17toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-17'));
            })->sum('quota_total');

        $date17ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-17'));
            })->sum('quota_total');

        $date17samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-17'));
            })->sum('quota_total');

        $date17surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-17'));
            })->sum('quota_total');


        $date17Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-17'));
            })->sum('des_ch');

        $date17Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-17'));
            })->sum('des_tao');

        $date17Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-17'));
            })->sum('des_ph');

        $date17Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-17'));
            })->sum('des_samui');

        $date17Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-17'));
            })->sum('des_surat');

        $date17Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-17'))
            ->sum('quota_total');

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date18ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-18'));
            })->sum('quota_total');

        $date18toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-18'));
            })->sum('quota_total');

        $date18ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-18'));
            })->sum('quota_total');

        $date18samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-18'));
            })->sum('quota_total');

        $date18surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-18'));
            })->sum('quota_total');


        $date18Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-18'));
            })->sum('des_ch');

        $date18Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-18'));
            })->sum('des_tao');

        $date18Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-18'));
            })->sum('des_ph');

        $date18Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-18'));
            })->sum('des_samui');

        $date18Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-18'));
            })->sum('des_surat');

        $date18Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-18'))
            ->sum('quota_total');


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date19ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-19'));
            })->sum('quota_total');

        $date19toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-19'));
            })->sum('quota_total');

        $date19ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-19'));
            })->sum('quota_total');

        $date19samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-19'));
            })->sum('quota_total');

        $date19surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-19'));
            })->sum('quota_total');


        $date19Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-19'));
            })->sum('des_ch');

        $date19Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-19'));
            })->sum('des_tao');

        $date19Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-19'));
            })->sum('des_ph');

        $date19Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-19'));
            })->sum('des_samui');

        $date19Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-19'));
            })->sum('des_surat');

        $date19Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-19'))
            ->sum('quota_total');

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date20ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-20'));
            })->sum('quota_total');

        $date20toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-20'));
            })->sum('quota_total');

        $date20ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-20'));
            })->sum('quota_total');

        $date20samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-20'));
            })->sum('quota_total');

        $date20surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-20'));
            })->sum('quota_total');


        $date20Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-20'));
            })->sum('des_ch');

        $date20Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-20'));
            })->sum('des_tao');

        $date20Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-20'));
            })->sum('des_ph');

        $date20Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-20'));
            })->sum('des_samui');

        $date20Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-20'));
            })->sum('des_surat');

        $date20Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-20'))
            ->sum('quota_total');


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date21ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-21'));
            })->sum('quota_total');

        $date21toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-21'));
            })->sum('quota_total');

        $date21ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-21'));
            })->sum('quota_total');

        $date21samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-21'));
            })->sum('quota_total');

        $date21surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-21'));
            })->sum('quota_total');


        $date21Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-21'));
            })->sum('des_ch');

        $date21Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-21'));
            })->sum('des_tao');

        $date21Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-21'));
            })->sum('des_ph');

        $date21Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-21'));
            })->sum('des_samui');

        $date21Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-21'));
            })->sum('des_surat');

        $date21Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-21'))
            ->sum('quota_total');


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date22ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-22'));
            })->sum('quota_total');

        $date22toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-22'));
            })->sum('quota_total');

        $date22ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-22'));
            })->sum('quota_total');

        $date22samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-22'));
            })->sum('quota_total');

        $date22surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-22'));
            })->sum('quota_total');


        $date22Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-22'));
            })->sum('des_ch');

        $date22Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-22'));
            })->sum('des_tao');

        $date22Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-22'));
            })->sum('des_ph');

        $date22Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-22'));
            })->sum('des_samui');

        $date22Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-22'));
            })->sum('des_surat');

        $date22Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-22'))
            ->sum('quota_total');

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date23ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-23'));
            })->sum('quota_total');

        $date23toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-23'));
            })->sum('quota_total');

        $date23ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-23'));
            })->sum('quota_total');

        $date23samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-23'));
            })->sum('quota_total');

        $date23surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-23'));
            })->sum('quota_total');


        $date23Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-23'));
            })->sum('des_ch');

        $date23Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-23'));
            })->sum('des_tao');

        $date23Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-23'));
            })->sum('des_ph');

        $date23Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-23'));
            })->sum('des_samui');

        $date23Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-23'));
            })->sum('des_surat');

        $date23Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-23'))
            ->sum('quota_total');


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date24ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-24'));
            })->sum('quota_total');

        $date24toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-24'));
            })->sum('quota_total');

        $date24ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-24'));
            })->sum('quota_total');

        $date24samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-24'));
            })->sum('quota_total');

        $date24surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-24'));
            })->sum('quota_total');


        $date24Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-24'));
            })->sum('des_ch');

        $date24Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-24'));
            })->sum('des_tao');

        $date24Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-24'));
            })->sum('des_ph');

        $date24Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-24'));
            })->sum('des_samui');

        $date24Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-24'));
            })->sum('des_surat');

        $date24Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-24'))
            ->sum('quota_total');


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date25ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-25'));
            })->sum('quota_total');

        $date25toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-25'));
            })->sum('quota_total');

        $date25ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-25'));
            })->sum('quota_total');

        $date25samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-25'));
            })->sum('quota_total');

        $date25surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-25'));
            })->sum('quota_total');


        $date25Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-25'));
            })->sum('des_ch');

        $date25Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-25'));
            })->sum('des_tao');

        $date25Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-25'));
            })->sum('des_ph');

        $date25Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-25'));
            })->sum('des_samui');

        $date25Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-25'));
            })->sum('des_surat');

        $date25Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-25'))
            ->sum('quota_total');


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date26ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-26'));
            })->sum('quota_total');

        $date26toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-26'));
            })->sum('quota_total');

        $date26ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-26'));
            })->sum('quota_total');

        $date26samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-26'));
            })->sum('quota_total');

        $date26surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-26'));
            })->sum('quota_total');


        $date26Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-26'));
            })->sum('des_ch');

        $date26Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-26'));
            })->sum('des_tao');

        $date26Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-26'));
            })->sum('des_ph');

        $date26Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-26'));
            })->sum('des_samui');

        $date26Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-26'));
            })->sum('des_surat');

        $date26Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-26'))
            ->sum('quota_total');

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date27ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-27'));
            })->sum('quota_total');

        $date27toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-27'));
            })->sum('quota_total');

        $date27ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-27'));
            })->sum('quota_total');

        $date27samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-27'));
            })->sum('quota_total');

        $date27surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-27'));
            })->sum('quota_total');


        $date27Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-27'));
            })->sum('des_ch');

        $date27Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-27'));
            })->sum('des_tao');

        $date27Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-27'));
            })->sum('des_ph');

        $date27Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-27'));
            })->sum('des_samui');

        $date27Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-27'));
            })->sum('des_surat');

        $date27Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-27'))
            ->sum('quota_total');

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date28ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-28'));
            })->sum('quota_total');

        $date28toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-2'));
            })->sum('quota_total');

        $date28ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-28'));
            })->sum('quota_total');

        $date28samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-28'));
            })->sum('quota_total');

        $date28surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-28'));
            })->sum('quota_total');


        $date28Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-28'));
            })->sum('des_ch');

        $date28Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-28'));
            })->sum('des_tao');

        $date28Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-28'));
            })->sum('des_ph');

        $date28Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-28'));
            })->sum('des_samui');

        $date28Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-28'));
            })->sum('des_surat');

        $date28Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-28'))
            ->sum('quota_total');


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date29ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-29'));
            })->sum('quota_total');

        $date29toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-29'));
            })->sum('quota_total');

        $date29ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-29'));
            })->sum('quota_total');

        $date29samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-29'));
            })->sum('quota_total');

        $date29surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-29'));
            })->sum('quota_total');


        $date29Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-29'));
            })->sum('des_ch');

        $date29Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-29'));
            })->sum('des_tao');

        $date29Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-29'));
            })->sum('des_ph');

        $date29Downsmui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-29'));
            })->sum('des_samui');

        $date29Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-29'));
            })->sum('des_surat');

        $date29Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-29'))
            ->sum('quota_total');


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date30ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-30'));
            })->sum('quota_total');

        $date30toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-30'));
            })->sum('quota_total');

        $date30ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-30'));
            })->sum('quota_total');

        $date30samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-30'));
            })->sum('quota_total');

        $date30surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-30'));
            })->sum('quota_total');


        $date30Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-30'));
            })->sum('des_ch');

        $date30Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-30'));
            })->sum('des_tao');

        $date30Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-30'));
            })->sum('des_ph');

        $date30Downsamui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-30'));
            })->sum('des_samui');

        $date30Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('Y-m-30'));
            })->sum('des_surat');

        $date30Total = DB::table('quota')
            ->where('quota_date', '=', date('Y-m-30'))
            ->sum('quota_total');

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $date31ch = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 1);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('2017-12-31'));
            })->sum('quota_total');

        $date31toa = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 2);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('2017-12-31'));
            })->sum('quota_total');

        $date31ph = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 3);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('2017-12-31'));
            })->sum('quota_total');

        $date31samui = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 4);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('2017-12-31'));
            })->sum('quota_total');

        $date31surat = DB::table('quota')
            ->where(function ($query) {
                $query->where('start_ID', '=', 5);
            })->where(function ($query) {
                $query->where('quota_date', '=', date('2017-12-31'));
            })->sum('quota_total');


        $date31Downch = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('2017-12-31'));
            })->sum('des_ch');

        $date31Downtao = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('2017-12-31'));
            })->sum('des_tao');

        $date31Downph = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('2017-12-31'));
            })->sum('des_ph');

        $date31Downsamui = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('2017-12-31'));
            })->sum('des_samui');

        $date31Downsarat = DB::table('quota')
            ->where(function ($query) {
            })->where(function ($query) {
                $query->where('quota_date', '=', date('2017-12-31'));
            })->sum('des_surat');

        $date31Total = DB::table('quota')
            ->where('quota_date', '=', date('2017-12-31'))
            ->sum('quota_total');


        return view('summaryLocation', [
            'summaryTotal' => $summaryTotal,

            'date1ch' => $date1ch,
            'date1toa' => $date1toa,
            'date1ph' => $date1ph,
            'date1samui' => $date1samui,
            'date1surat' => $date1surat,
            'date1Downch' => $date1Downch,
            'date1Downtao' => $date1Downtao,
            'date1Downph' => $date1Downph,
            'date1Downsmui' => $date1Downsmui,
            'date1Downsarat' => $date1Downsarat,
            'date1Total' => $date1Total,

            'date2ch' => $date2ch,
            'date2toa' => $date2toa,
            'date2ph' => $date2ph,
            'date2samui' => $date2samui,
            'date2surat' => $date2surat,
            'date2Downch' => $date2Downch,
            'date2Downtao' => $date2Downtao,
            'date2Downph' => $date2Downph,
            'date2Downsmui' => $date2Downsmui,
            'date2Downsarat' => $date2Downsarat,
            'date2Total' => $date2Total,

            'date3ch' => $date3ch,
            'date3toa' => $date3toa,
            'date3ph' => $date3ph,
            'date3samui' => $date3samui,
            'date3surat' => $date3surat,
            'date3Downch' => $date3Downch,
            'date3Downtao' => $date3Downtao,
            'date3Downph' => $date3Downph,
            'date3Downsamui' => $date3Downsamui,
            'date3Downsarat' => $date3Downsarat,
            'date3Total' => $date3Total,

            'date4ch' => $date4ch,
            'date4toa' => $date4toa,
            'date4ph' => $date4ph,
            'date4samui' => $date4samui,
            'date4surat' => $date4surat,
            'date4Downch' => $date4Downch,
            'date4Downtao' => $date4Downtao,
            'date4Downph' => $date4Downph,
            'date4Downsmui' => $date4Downsmui,
            'date4Downsarat' => $date4Downsarat,
            'date4Total' => $date4Total,

            'date5ch' => $date5ch,
            'date5toa' => $date5toa,
            'date5ph' => $date5ph,
            'date5samui' => $date5samui,
            'date5surat' => $date5surat,
            'date5Downch' => $date5Downch,
            'date5Downtao' => $date5Downtao,
            'date5Downph' => $date5Downph,
            'date5Downsmui' => $date5Downsmui,
            'date5Downsarat' => $date5Downsarat,
            'date5Total' => $date5Total,

            'date6ch' => $date6ch,
            'date6toa' => $date6toa,
            'date6ph' => $date6ph,
            'date6samui' => $date6samui,
            'date6surat' => $date6surat,
            'date6Downch' => $date6Downch,
            'date6Downtao' => $date6Downtao,
            'date6Downph' => $date6Downph,
            'date6Downsmui' => $date6Downsmui,
            'date6Downsarat' => $date6Downsarat,
            'date6Total' => $date6Total,

            'date7ch' => $date7ch,
            'date7toa' => $date7toa,
            'date7ph' => $date7ph,
            'date7samui' => $date7samui,
            'date7surat' => $date7surat,
            'date7Downch' => $date7Downch,
            'date7Downtao' => $date7Downtao,
            'date7Downph' => $date7Downph,
            'date7Downsmui' => $date7Downsmui,
            'date7Downsarat' => $date7Downsarat,
            'date7Total' => $date7Total,

            'date8ch' => $date8ch,
            'date8toa' => $date8toa,
            'date8ph' => $date8ph,
            'date8samui' => $date8samui,
            'date8surat' => $date8surat,
            'date8Downch' => $date8Downch,
            'date8Downtao' => $date8Downtao,
            'date8Downph' => $date8Downph,
            'date8Downsmui' => $date8Downsmui,
            'date8Downsarat' => $date8Downsarat,
            'date8Total' => $date8Total,

            'date9ch' => $date9ch,
            'date9toa' => $date9toa,
            'date9ph' => $date9ph,
            'date9samui' => $date9samui,
            'date9surat' => $date9surat,
            'date9Downch' => $date9Downch,
            'date9Downtao' => $date9Downtao,
            'date9Downph' => $date9Downph,
            'date9Downsmui' => $date9Downsmui,
            'date9Downsarat' => $date9Downsarat,
            'date9Total' => $date9Total,

            'date10ch' => $date10ch,
            'date10toa' => $date10toa,
            'date10ph' => $date10ph,
            'date10samui' => $date10samui,
            'date10surat' => $date10surat,
            'date10Downch' => $date10Downch,
            'date10Downtao' => $date10Downtao,
            'date10Downph' => $date10Downph,
            'date10Downsmui' => $date10Downsmui,
            'date10Downsarat' => $date10Downsarat,
            'date10Total' => $date10Total,

            'date11ch' => $date11ch,
            'date11toa' => $date11toa,
            'date11ph' => $date11ph,
            'date11samui' => $date11samui,
            'date11surat' => $date11surat,
            'date11Downch' => $date11Downch,
            'date11Downtao' => $date11Downtao,
            'date11Downph' => $date11Downph,
            'date11Downsmui' => $date11Downsmui,
            'date11Downsarat' => $date11Downsarat,
            'date11Total' => $date11Total,

            'date12ch' => $date12ch,
            'date12toa' => $date12toa,
            'date12ph' => $date12ph,
            'date12samui' => $date12samui,
            'date12surat' => $date12surat,
            'date12Downch' => $date12Downch,
            'date12Downtao' => $date12Downtao,
            'date12Downph' => $date12Downph,
            'date12Downsmui' => $date12Downsmui,
            'date12Downsarat' => $date12Downsarat,
            'date12Total' => $date12Total,

            'date13ch' => $date13ch,
            'date13toa' => $date13toa,
            'date13ph' => $date13ph,
            'date13samui' => $date13samui,
            'date13surat' => $date13surat,
            'date13Downch' => $date13Downch,
            'date13Downtao' => $date13Downtao,
            'date13Downph' => $date13Downph,
            'date13Downsmui' => $date13Downsmui,
            'date13Downsarat' => $date13Downsarat,
            'date13Total' => $date13Total,

            'date14ch' => $date14ch,
            'date14toa' => $date14toa,
            'date14ph' => $date14ph,
            'date14samui' => $date14samui,
            'date14surat' => $date14surat,
            'date14Downch' => $date14Downch,
            'date14Downtao' => $date14Downtao,
            'date14Downph' => $date14Downph,
            'date14Downsmui' => $date14Downsmui,
            'date14Downsarat' => $date14Downsarat,
            'date14Total' => $date14Total,

            'date15ch' => $date15ch,
            'date15toa' => $date15toa,
            'date15ph' => $date15ph,
            'date15samui' => $date15samui,
            'date15surat' => $date15surat,
            'date15Downch' => $date15Downch,
            'date15Downtao' => $date15Downtao,
            'date15Downph' => $date15Downph,
            'date15Downsmui' => $date15Downsmui,
            'date15Downsarat' => $date15Downsarat,
            'date15Total' => $date15Total,

            'date16ch' => $date16ch,
            'date16toa' => $date16toa,
            'date16ph' => $date16ph,
            'date16samui' => $date16samui,
            'date16surat' => $date16surat,
            'date16Downch' => $date16Downch,
            'date16Downtao' => $date16Downtao,
            'date16Downph' => $date16Downph,
            'date16Downsmui' => $date16Downsmui,
            'date16Downsarat' => $date16Downsarat,
            'date16Total' => $date16Total,

            'date17ch' => $date17ch,
            'date17toa' => $date17toa,
            'date17ph' => $date17ph,
            'date17samui' => $date17samui,
            'date17surat' => $date17surat,
            'date17Downch' => $date17Downch,
            'date17Downtao' => $date17Downtao,
            'date17Downph' => $date17Downph,
            'date17Downsmui' => $date17Downsmui,
            'date17Downsarat' => $date17Downsarat,
            'date17Total' => $date17Total,

            'date18ch' => $date18ch,
            'date18toa' => $date18toa,
            'date18ph' => $date18ph,
            'date18samui' => $date18samui,
            'date18surat' => $date18surat,
            'date18Downch' => $date18Downch,
            'date18Downtao' => $date18Downtao,
            'date18Downph' => $date18Downph,
            'date18Downsmui' => $date18Downsmui,
            'date18Downsarat' => $date18Downsarat,
            'date18Total' => $date18Total,

            'date19ch' => $date19ch,
            'date19toa' => $date19toa,
            'date19ph' => $date19ph,
            'date19samui' => $date19samui,
            'date19surat' => $date19surat,
            'date19Downch' => $date19Downch,
            'date19Downtao' => $date19Downtao,
            'date19Downph' => $date19Downph,
            'date19Downsmui' => $date19Downsmui,
            'date19Downsarat' => $date19Downsarat,
            'date19Total' => $date19Total,

            'date20ch' => $date20ch,
            'date20toa' => $date20toa,
            'date20ph' => $date20ph,
            'date20samui' => $date20samui,
            'date20surat' => $date20surat,
            'date20Downch' => $date20Downch,
            'date20Downtao' => $date20Downtao,
            'date20Downph' => $date20Downph,
            'date20Downsmui' => $date20Downsmui,
            'date20Downsarat' => $date20Downsarat,
            'date20Total' => $date20Total,

            'date21ch' => $date21ch,
            'date21toa' => $date21toa,
            'date21ph' => $date21ph,
            'date21samui' => $date21samui,
            'date21surat' => $date21surat,
            'date21Downch' => $date21Downch,
            'date21Downtao' => $date21Downtao,
            'date21Downph' => $date21Downph,
            'date21Downsmui' => $date21Downsmui,
            'date21Downsarat' => $date21Downsarat,
            'date21Total' => $date21Total,

            'date22ch' => $date22ch,
            'date22toa' => $date22toa,
            'date22ph' => $date22ph,
            'date22samui' => $date22samui,
            'date22surat' => $date22surat,
            'date22Downch' => $date22Downch,
            'date22Downtao' => $date22Downtao,
            'date22Downph' => $date22Downph,
            'date22Downsmui' => $date22Downsmui,
            'date22Downsarat' => $date22Downsarat,
            'date22Total' => $date22Total,

            'date23ch' => $date23ch,
            'date23toa' => $date23toa,
            'date23ph' => $date23ph,
            'date23samui' => $date23samui,
            'date23surat' => $date23surat,
            'date23Downch' => $date23Downch,
            'date23Downtao' => $date23Downtao,
            'date23Downph' => $date23Downph,
            'date23Downsmui' => $date23Downsmui,
            'date23Downsarat' => $date23Downsarat,
            'date23Total' => $date23Total,

            'date24ch' => $date24ch,
            'date24toa' => $date24toa,
            'date24ph' => $date24ph,
            'date24samui' => $date24samui,
            'date24surat' => $date24surat,
            'date24Downch' => $date24Downch,
            'date24Downtao' => $date24Downtao,
            'date24Downph' => $date24Downph,
            'date24Downsmui' => $date24Downsmui,
            'date24Downsarat' => $date24Downsarat,
            'date24Total' => $date24Total,

            'date25ch' => $date25ch,
            'date25toa' => $date25toa,
            'date25ph' => $date25ph,
            'date25samui' => $date25samui,
            'date25surat' => $date25surat,
            'date25Downch' => $date25Downch,
            'date25Downtao' => $date25Downtao,
            'date25Downph' => $date25Downph,
            'date25Downsmui' => $date25Downsmui,
            'date25Downsarat' => $date25Downsarat,
            'date25Total' => $date25Total,

            'date26ch' => $date25ch,
            'date26toa' => $date25toa,
            'date26ph' => $date25ph,
            'date26samui' => $date25samui,
            'date26surat' => $date25surat,
            'date26Downch' => $date25Downch,
            'date26Downtao' => $date25Downtao,
            'date26Downph' => $date25Downph,
            'date26Downsmui' => $date25Downsmui,
            'date26Downsarat' => $date25Downsarat,
            'date26Total' => $date25Total,

            'date27ch' => $date27ch,
            'date27toa' => $date27toa,
            'date27ph' => $date27ph,
            'date27samui' => $date27samui,
            'date27surat' => $date27surat,
            'date27Downch' => $date27Downch,
            'date27Downtao' => $date27Downtao,
            'date27Downph' => $date27Downph,
            'date27Downsmui' => $date27Downsmui,
            'date27Downsarat' => $date27Downsarat,
            'date27Total' => $date27Total,

            'date28ch' => $date28ch,
            'date28toa' => $date28toa,
            'date28ph' => $date28ph,
            'date28samui' => $date28samui,
            'date28surat' => $date28surat,
            'date28Downch' => $date28Downch,
            'date28Downtao' => $date28Downtao,
            'date28Downph' => $date28Downph,
            'date28Downsmui' => $date28Downsmui,
            'date28Downsarat' => $date28Downsarat,
            'date28Total' => $date28Total,

            'date29ch' => $date29ch,
            'date29toa' => $date29toa,
            'date29ph' => $date29ph,
            'date29samui' => $date29samui,
            'date29surat' => $date29surat,
            'date29Downch' => $date29Downch,
            'date29Downtao' => $date29Downtao,
            'date29Downph' => $date29Downph,
            'date29Downsmui' => $date29Downsmui,
            'date29Downsarat' => $date29Downsarat,
            'date29Total' => $date29Total,

            'date30ch' => $date30ch,
            'date30toa' => $date30toa,
            'date30ph' => $date30ph,
            'date30samui' => $date30samui,
            'date30surat' => $date30surat,
            'date30Downch' => $date30Downch,
            'date30Downtao' => $date30Downtao,
            'date30Downph' => $date30Downph,
            'date30Downsamui' => $date30Downsamui,
            'date30Downsarat' => $date30Downsarat,
            'date30Total' => $date30Total,
//month : 12

            'date31ch' => $date31ch,
            'date31toa' => $date31toa,
            'date31ph' => $date31ph,
            'date31samui' => $date31samui,
            'date31surat' => $date31surat,
            'date31Downch' => $date31Downch,
            'date31Downtao' => $date31Downtao,
            'date31Downph' => $date31Downph,
            'date31Downsamui' => $date31Downsamui,
            'date31Downsarat' => $date31Downsarat,
            'date31Total' => $date31Total,
        ]);

    }
}
