<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class selectSummaryBoat extends Controller
{
    function SummaryBoat(Request $request)
    {


        $year = date('Y');
        $month2 = date('m');
        $day = 01;
        $lastDay = [];
        $selBoat = [];
        $selBoatSu01 = [];
        $selR501 = [];
        $selR5Su01 = [];
        $selR901 = [];
        $selR9Su01 = [];
        $sum = [];
        $m = $request->input('se');


        for ($month = 1; $month <= 12; $month++) {
            $today = $year . '-' . $month . '-' . $day;
            $lastDay[] = date('t', strtotime($today));


            //print_r($lastDay);
            //$end1 = $lastDay[0];

        }
        if ($m != "") {
            $i = $m - 1;
        } else {
            $i = $month2 - 1;
        }
        for ($day = 1; $day <= $lastDay[$i]; $day++) {

            $query = DB::table('quota')
                ->where('boat_ID', '=', 1)
                ->whereIN('route_ID', [3, 4]);

            if ($m != "") {

                $query->where('quota_date', '=', date($year . '-' . $m . '-' . $day));
            } else {
                $query->where('quota_date', '=', date($year . '-' . $month2 . '-' . $day));
            }
            $selBoat[] = $query->sum('quota_total');


            $query = DB::table('quota')
                ->where('boat_ID', '=', 1)
                ->whereIN('route_ID', [1, 2]);
            if ($m != "") {
                $query->where('quota_date', '=', date($year . '-' . $m . '-' . $day));
            } else {
                $query->where('quota_date', '=', date($year . '-' . $month2 . '-' . $day));
            }
            $selBoatSu01[] = $query->sum('quota_total');


            $query = DB::table('quota')
                ->where('boat_ID', '=', 2)
                ->whereIN('route_ID', [3, 4]);
            if ($m != "") {
                $query->where('quota_date', '=', date($year . '-' . $m . '-' . $day));
            } else {
                $query->where('quota_date', '=', date($year . '-' . $month2 . '-' . $day));
            }
            $selR501[] = $query->sum('quota_total');


            $query = DB::table('quota')
                ->where('boat_ID', '=', 2)
                ->whereIN('route_ID', [1, 2]);
            if ($m != "") {
                $query->where('quota_date', '=', date($year . '-' . $m . '-' . $day));
            } else {
                $query->where('quota_date', '=', date($year . '-' . $month2 . '-' . $day));
            }
            $selR5Su01[] = $query->sum('quota_total');


            $query = DB::table('quota')
                ->where('boat_ID', '=', 3)
                ->whereIN('route_ID', [3, 4]);
            if ($m != "") {
                $query->where('quota_date', '=', date($year . '-' . $m . '-' . $day));
            } else {
                $query->where('quota_date', '=', date($year . '-' . $month2 . '-' . $day));
            }
            $selR901[] = $query->sum('quota_total');


            $query = DB::table('quota')
                ->where('boat_ID', '=', 3)
                ->whereIN('route_ID', [1, 2]);
            if ($m != "") {
                $query->where('quota_date', '=', date($year . '-' . $m . '-' . $day));
            } else {
                $query->where('quota_date', '=', date($year . '-' . $month2 . '-' . $day));
            }
            $selR9Su01[] = $query->sum('quota_total');


            $sum = $lastDay[$i];
        }

        //    print_r($sum);
        //   exit();


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $total = DB::table('quota')
            ->sum('quota_total');
        if ($m != "") {
            $countBoat1 = DB::table('quota')
                ->where('boat_ID', '=', 1)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '15'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $countBoat1 = DB::table('quota')
                ->where('boat_ID', '=', 1)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '15'))
                ->count(DB::raw('DISTINCT quota_date'));
        }

        if ($m != "") {
            $countBoat2 = DB::table('quota')
                ->where('boat_ID', '=', 1)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '15'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $countBoat2 = DB::table('quota')
                ->where('boat_ID', '=', 1)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '15'))
                ->count(DB::raw('DISTINCT quota_date'));
        }


        if ($m != "") {
            $countBoat3 = DB::table('quota')
                ->where('boat_ID', '=', 2)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '15'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $countBoat3 = DB::table('quota')
                ->where('boat_ID', '=', 2)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '15'))
                ->count(DB::raw('DISTINCT quota_date'));
        }

        if ($m != "") {
            $countBoat4 = DB::table('quota')
                ->where('boat_ID', '=', 2)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '15'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $countBoat4 = DB::table('quota')
                ->where('boat_ID', '=', 2)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '15'))
                ->count(DB::raw('DISTINCT quota_date'));
        }

        if ($m != "") {
            $countBoat5 = DB::table('quota')
                ->where('boat_ID', '=', 3)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '15'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $countBoat5 = DB::table('quota')
                ->where('boat_ID', '=', 3)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '15'))
                ->count(DB::raw('DISTINCT quota_date'));
        }

        if ($m != "") {
            $countBoat6 = DB::table('quota')
                ->where('boat_ID', '=', 3)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '15'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $countBoat6 = DB::table('quota')
                ->where('boat_ID', '=', 3)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '15'))
                ->count(DB::raw('DISTINCT quota_date'));
        }

        if ($m != "") {
            $countBoat7 = DB::table('quota')
                ->where('boat_ID', '=', 1)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '16'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $countBoat7 = DB::table('quota')
                ->where('boat_ID', '=', 1)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '16'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        }

        if ($m != "") {
            $countBoat8 = DB::table('quota')
                ->where('boat_ID', '=', 1)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '16'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $countBoat8 = DB::table('quota')
                ->where('boat_ID', '=', 1)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '16'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        }
        if ($m != "") {
            $countBoat9 = DB::table('quota')
                ->where('boat_ID', '=', 2)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '16'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $countBoat9 = DB::table('quota')
                ->where('boat_ID', '=', 2)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '16'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        }

        if ($m != "") {
            $countBoat10 = DB::table('quota')
                ->where('boat_ID', '=', 2)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '16'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $countBoat10 = DB::table('quota')
                ->where('boat_ID', '=', 2)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '16'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        }

        if ($m != "") {
            $countBoat11 = DB::table('quota')
                ->where('boat_ID', '=', 3)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '16'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $countBoat11 = DB::table('quota')
                ->where('boat_ID', '=', 3)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '16'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        }

        if ($m != "") {
            $countBoat12 = DB::table('quota')
                ->where('boat_ID', '=', 3)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '16'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $countBoat12 = DB::table('quota')
                ->where('boat_ID', '=', 3)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '16'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        }

////////////////////////////////////////////////////count//////////////////////////////////////////////////////////////


        if ($m != "") {
            $R301 = DB::table('quota')
                ->where('boat_ID', '=', 1)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $R301 = DB::table('quota')
                ->where('boat_ID', '=', 1)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        }

        if ($m != "") {
            $R302 = DB::table('quota')
                ->where('boat_ID', '=', 1)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $R302 = DB::table('quota')
                ->where('boat_ID', '=', 1)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        }

        if ($m != "") {
            $TotalR3 = DB::table('quota')
                ->where('boat_ID', '=', 1)
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $TotalR3 = DB::table('quota')
                ->where('boat_ID', '=', 1)
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        }
        if ($m != "") {
            $R501 = DB::table('quota')
                ->where('boat_ID', '=', 2)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $R501 = DB::table('quota')
                ->where('boat_ID', '=', 2)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        }
        if ($m != "") {
            $R502 = DB::table('quota')
                ->where('boat_ID', '=', 2)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $R502 = DB::table('quota')
                ->where('boat_ID', '=', 2)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        }

        if ($m != "") {
            $TotalR5 = DB::table('quota')
                ->where('boat_ID', '=', 2)
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $TotalR5 = DB::table('quota')
                ->where('boat_ID', '=', 2)
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        }
        if ($m != "") {
            $R901 = DB::table('quota')
                ->where('boat_ID', '=', 3)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $R901 = DB::table('quota')
                ->where('boat_ID', '=', 3)
                ->whereIN('route_ID', [3, 4])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        }
        if ($m != "") {
            $R902 = DB::table('quota')
                ->where('boat_ID', '=', 3)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $R902 = DB::table('quota')
                ->where('boat_ID', '=', 3)
                ->whereIN('route_ID', [1, 2])
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        }
        if ($m != "") {
            $TotalR9 = DB::table('quota')
                ->where('boat_ID', '=', 3)
                ->Where('quota_date', '>=', date($year . '-' . $m . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $m . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        } else {
            $TotalR9 = DB::table('quota')
                ->where('boat_ID', '=', 3)
                ->Where('quota_date', '>=', date($year . '-' . $month2 . '-' . '1'))
                ->Where('quota_date', '<=', date($year . '-' . $month2 . '-' . '31'))
                ->count(DB::raw('DISTINCT quota_date'));
        }


        $summaryTotal = DB::table('quota')
            ->sum('quota_total');


        return view('summaryBoat',
            [
                'selBoat' => $selBoat,
                'selBoatSu01' => $selBoatSu01,
                'selR501' => $selR501,
                'selR5Su01' => $selR5Su01,
                'selR901' => $selR901,
                'selR9Su01' => $selR9Su01,

                'total' => $total,
                'summaryTotal' => $summaryTotal,
                'countBoat1' => $countBoat1,
                'countBoat2' => $countBoat2,
                'countBoat3' => $countBoat3,
                'countBoat4' => $countBoat4,
                'countBoat5' => $countBoat5,
                'countBoat6' => $countBoat6,

                'countBoat7' => $countBoat7,
                'countBoat8' => $countBoat8,
                'countBoat9' => $countBoat9,
                'countBoat10' => $countBoat10,
                'countBoat11' => $countBoat11,
                'countBoat12' => $countBoat12,
                'R301' => $R301,
                'R302' => $R302,
                'TotalR3' => $TotalR3,
                'R501' => $R501,
                'R502' => $R502,
                'TotalR5' => $TotalR5,
                'R901' => $R901,
                'R902' => $R902,
                'TotalR9' => $TotalR9,
                'sum' => $sum,
            ]);


    }
}
